// #### Task 1 💻
// Написать перебор от 1 до 20, где выведутся все числа кратные трём.

    for (let i = 1; i <= 20; i++) {
        if (i % 3 === 0) console.log(i);
    }

    for (let i = 3; i <= 20; i += 3) {
        console.log(i);
    }


// #### Task 2 💻
// Нам надо идти на работу, но нужно проверить все ли мы взяли или нет?
// Нам точно нужны ключи документы и ручка, но из еды нам надо яблоко или апельсин. 

    let key = true
    let documents = true
    let pen = true
    let apple = false
    let orange = true

    shouldGoToWork = key && documents && pen && (apple || orange);
    console.log(shouldGoToWork);



// #### Task 3 (boolean operators, conditionals)
// Напишите программу, которая спрашивает у пользователя номер года, а затем выводит на 
// экран, високосный ли это год.

    let year = prompt('Enter year');

    if ((year % 4 === 0 && year % 100 !== 0) || year % 400 === 0) {
        alert("It's a leap year");
    } else {
        alert("It's not a leap year");
    }



// #### Task 4 💻
// Запросить у пользователя число: 
//     1. Если число делится без остатка на 5 выводим сообщение Fiz
//     2. Если число делится без остатка на 3 выводим сообшение Buz
//     3. Если число делится без остатка и на 3 и на 5 выводим сообшение FizBuz

    let num = prompt('Enter number');
    let message = '';

    if (num % 5 === 0) {
        message += 'Fiz';
    }

    if (num % 3 === 0) {
        message += 'Buz';
    }

    console.log(message);


    //второй вариант
    let num = prompt('Enter number');
    let message = '';

    switch (true) {
        case num % 5 === 0 && num % 3 === 0:
            message = 'FizBuz';
            break;
        case num % 5 === 0: 
            message = 'Fiz';
            break;
        case num % 3 === 0: 
            message = 'Buz';
            break;
        default:
            console.error('Entered invalid number');
    }

    console.log(message);

// #### Task 6 💻
// Используя конструкцию `switch`, напишите программу, которая спрашивает у пользователя 
// номер месяца и года, а затем выводит на экран количество дней в этом месяце.
// Если номер месяца введён неверно - напишите об этом в консоль

    let month = Number(prompt('Enter month'));
    let year = Number(prompt('Enter year'));

    switch (month) {
        case 1: case 3: case 5: case 7: case 8: case 10: case 12:
            console.log('31 days');
            break;
        case 4: case 6: case 9: case 11:
            console.log('30 days');
            break;
        case 2:
            if ((year % 4 == 0 && year % 100 !== 0) || year % 400 == 0) {
                console.log('29 days');
            } else {
                console.log('28 days');
            }
            break;
        default:
            console.error('Entered invalid month number');
    }



// ### ADVANCED level

// #### Task 1 👨‍🏫 
// Напишите программу, которая выполняет следующие операции: 
// 1. Запрашивает у пользователя число.
// 2. Последовательно задает вопрос: 
//     cколько отнять / прибавить / умножить / разделить от предыдущего результата?
// 3. По окончании вывести пользователю **`alert`**, содержащий формулу и результат.

    //первый вариант

    result = prompt('Enter number');
    formula = `${result}`;

    userNum = Number(prompt('Subtract'));
    formula = `(${formula} - ${userNum})`;
    result -= userNum;

    userNum = Number(prompt('Add'));
    formula = `(${formula} + ${userNum})`;
    result += userNum;

    userNum = Number(prompt('Multiply by'));
    formula = `(${formula} * ${userNum})`;
    result *= userNum;

    userNum = Number(prompt('Divided by'));
    formula = `${formula} / ${userNum}`;
    result /= userNum;

    alert(`${formula} = ${result}`);


    // второй вариант
    let result;
    let userNum;
    let formula = '';

    let questions = ['Subtract', 'Add', 'Multiply by', 'Divided by'];
    let mathSymbols = ['-', '+', '*', '/'];

    result = Number(prompt('Enter number'));

    for (let i = 0; i < questions.length; i++) {
        userNum = Number(prompt(questions[i]));
        result = `(${result}) ${mathSymbols[i]} ${userNum}`;
    }

    alert(`${result} = ${eval(result)}`);



// #### Task 2 👨‍🏫
// Написать программу, которая будет выводить в консоль лесенку.

    let stair = '#';
    let stairsCount = prompt('How many stairs you want?');

    for (let i = 0; i < stairsCount; i++) {
        console.log(stair);
        stair += '#';
    }



// #### Task 3 👨‍🏫 
// Дополните результат задания 6 так, чтобы программа спрашивала пользователя до тех пор, 
// пока он не введёт корректное значение или напишет в окошко `prompt()` специальное стоп-слово.

    let month;
    let year;

    while (true) {
        month = prompt('Enter month');

        if (month == 'stop') {
            break;
        }
        
        year = Number(prompt('Enter year'));

        switch (Number(month)) {
            case 1: case 3: case 5: case 7: case 8: case 10: case 12:
                console.log('31 days');
                break;
            case 4: case 6: case 9: case 11:
                console.log('30 days');
                break;
            case 2:
                if ((year % 4 == 0 && year % 100 !== 0) || year % 400 == 0) {
                    console.log('29 days');
                } else {
                    console.log('28 days');
                }
                break;
            default:
                console.error('Entered invalid month number');
        }
    }
