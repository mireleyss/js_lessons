// ### NORMAL level

// #### Task 1 💻
// Написать функцию **getSum**, которая будет высчитывать сумму чисел от нуля, до параметра, который мы в неё передаем. 
// > Если передадим число 100 то, надо вычислить сумму чисел от 0 до 100 (должно получится 5050)

function getSum(userNum) {
    let sum = 0;

    for (let i = 0; i <= userNum; i++) {
        sum += i;
    }

    return sum;
}

let userNum = prompt('Enter your number');

console.log(getSum(userNum));



// #### Task 2 💻
// Напишите функцию которая в качестве аргумента принимает в себя сумму кредита который хочет получить клиент и верните результат переплаты по кредиту:
// + процентная ставка в год — 17%,
// + количество лет — 5.

function findOverpayment(loanAmount) {
    const INTEREST_RATE = 0.17;
    const MATURIRY = 5;

    overpayment = loanAmount * INTEREST_RATE * MATURIRY;

    return overpayment.toFixed(2);
}

let loanAmount = prompt('Enter loan amount');

console.log(findOverpayment(loanAmount));



// #### Task 3 💻
// Написать функцию **`getSum`** которая принимает два целых числа a и b, которые могут быть положительными или отрицательными, найти сумму всех чисел между ними, включая их, и вернуть ее. Если два числа равны, верните a или b.


function getSum(a, b) {
    if (a > b) {
        temp = a;
        a = b;
        b = temp;
    } else if (a == b) {
        return a;
    }

    let sum = 0;

    for (i = a; i <= b; i++) {
        sum += i;
    }

    return sum;
}

let a = Number(prompt('Enter first number'));
let b = Number(prompt('Enter second number'));

console.log(getSum(a, b));



// #### Task 4 💻
// Напишите функцию **fooboo** которая принимает в качестве аргумента три параметра:
// + булевое значение
// + функцию **foo** которая выводит в консоль 'foo'
// + функцию **boo** которая выводит в консоль 'boo'
// > Если переданное булевое значение **true** запускаем функцию **foo** иначе **boo**


function fooboo(is_true, foo, boo) {
    is_true ? foo() : boo();
}

function foo() {
    console.log('foo');
}

function boo() {
    console.log('boo');
}

fooboo(true, foo, boo);



// #### Task 5 💻
// Напишите функцию **withNumberArgs()**, которая будет декорировать любую функцию
// от двух переменных, которую вы в неё передадите.
// Декорированная функция должна выводить в консоль ошибку, если тип одного из аргументов
// не равен числу, и возвращать `0`:


function withNumberArgs(func) {
    return function (a, b) {
        if (typeof a !== 'number' || typeof b !== 'number') {
            console.log('Incorrect parameter type');
            return 0;
        }
        return func(a, b);
    }
}

const mul = (a, b) => a * b;
const safeMul = withNumberArgs(mul);

console.log(safeMul(5, 5));
console.log(safeMul(5, "5"));



// ### ADVANCED level
// #### Task 1 👨‍🏫 
// + Реализуйте функцию, который принимает 3 целочисленных значения a, b, c. Функция должна возвращать **true**, если треугольник можно построить со сторонами заданной длины, и **false** в любом другом случае.


function triangleCheck(a, b, c) {
    if (a + b <= c || a + c <= b || b + c <= a) {
        return false;
    }

    return true;
}

console.log(triangleCheck(10, 3, 8));
console.log(triangleCheck(5, 13, 8));



// #### Task 2 👨‍🏫
// + Напишите программу для вычисления общей стоимости покупки телефонов. Вы будете продолжать покупать телефоны (подсказка: циклы!), пока у вас не закончатся деньги на банковском счете. Вы также будете покупать аксессуары для каждого из телефонов.
// + После того, как вы посчитаете сумму покупки, прибавьте налог, затем выведите на экран вычисленную сумму покупки, правильно отформатировав её.
// + Наконец, сверьте сумму с балансом вашего банковского счета, чтобы понять можете вы себе это позволить или нет.
// + Вы должны настроить некоторые константы для «ставки налога», «цены телефона», «цены аксессуара», также как и переменную для вашего «баланса банковского счета».
// + Вам следует определить функции для вычисления налога и для форматирования цены со знаком валюты и округлением до двух знаков после запятой.
// + Как последний этап, попробуйте включить ввод данных в вашу программу, например с помощью функции prompt(..). Вы можете, например, запросить у пользователя баланс банковского счета. Развлекайтесь и будьте изобретательны!


const TAX_RATE = 0.02;
const PHONE_PRICE = 1500.90;
const ACCESSORY_PRICE = 5.90;
//ввод с консоли
// const PHONE_PRICE = Number(prompt('Enter phone price'));
// const ACCESSORY_PRICE = Number(prompt('Enter accessory price'));
const OVERALL_COST = PHONE_PRICE + ACCESSORY_PRICE;

let balance = Number(prompt('Enter your balance'));

function calculatePriceWithTax(totalPrice) {
    return totalPrice += totalPrice * TAX_RATE;
}

function formatPrice(price) {
    return `$${price.toFixed(2)}`;
}

function getTotalPrice(balance) {
    let totalPrice = 0;

    while (OVERALL_COST < balance) {
        totalPrice += OVERALL_COST;
        balance -= OVERALL_COST;
    }

    return totalPrice;
}

function checkAffordance(totalPrice, balance) {
    totalCost = calculatePriceWithTax(totalPrice);

    console.log(`Total cost is ${formatPrice(totalCost)}`);

    if (totalCost < balance) {
        console.log('You can afford this much phones');
        return true;
    }

    console.log('You can\'t afford this much phones');
    return false;
}

let totalCost = getTotalPrice(balance);
console.log(checkAffordance(totalCost, balance));


// #### Task 3 👨‍🏫 - дополнительно
// + Ваша задача - разбить плитку шоколада заданного размера n x m на маленькие квадраты. Каждый квадрат имеет размер 1x1 и не может быть разбит. Реализуйте функцию, которая будет возвращать минимальное количество необходимых надломов.
// + Например, если вам дается плитка шоколада размером 2 x 1, вы можете разделить ее на отдельные квадраты всего за один надлом, но для размера 3 x 1 вы должны сделать два надлома.
// + Если входные данные недействительны, вы должны вернуть 0 (поскольку надломы не требуются, если у нас нет шоколада для разделения). Ввод всегда будет неотрицательным целым числом. */}

function getBreaksNum(a, b) {
    if (a !== 0 && b !== 0 && (a !== 1 || b !== 1)) return a * b - 1;

    console.log('Can\'t divide');
    return 0;
}

console.log(getBreaksNum(4, 5));
console.log(getBreaksNum(1, 2));
console.log(getBreaksNum(1, 1));