#### Task 1 💻
Создайте переменные, затем сложите их и выведите результат в консоль разработчика.

    const X = 20;
    const Y = 58;
    const Z = 42;

    console.log(X + Y + Z);



#### Task 2 💻
Создайте константы:
    - количество секунд в минуте
    - количество минут в часу
    - количество часов в сутках
    - количество суток в году 
Посчитайте ваш возраст в секундах и поместите результат в переменную **`myAgeInSeconds`**

    const SEC_IN_MIN = 60;
    const MIN_IN_HR = 60;
    const HRS_IN_DAY = 24;
    const DAYS_IN_YEAR = 365;

    let age = 19;

    let myAgeInSeconds = age * DAYS_IN_YEAR * HRS_IN_DAY * MIN_IN_HR * SEC_IN_MIN;

    console.log(myAgeInSeconds);



#### Task 3 💻
Создайте две переменные. Поместите в них переменную **`count`** и превратите в строку, а **`userName`** наоборот в число. Попробуйте реализовать задачу двумя разными способами.

    let count = 42;
    let userName = '42';

    let countToStringFirst = String(count);
    let countToStringSecond = count + ''; //второй вариант

    console.log(typeof countToStringFirst);
    console.log(typeof countToStringSecond);

    let userNameToIntFirst = Number(userName);
    let userNameToIntSecond = userName * 1; //второй вариант

    console.log(typeof userNameToIntFirst);
    console.log(typeof userNameToIntSecond);



    #### Task 4 💻  
Создать 3 переменные разных типов и вывести в консоль для каждой из них строку следующего вида:    
    `Variable value: %variable value% has type: %type variable%`

    let animal = 'Frog';
    let legsCount = 4;
    let isMammal = false;

    console.log(`Variable value: ${animal} has type: ${typeof animal}`);
    console.log(`Variable value: ${legsCount} has type: ${typeof legsCount}`);
    console.log(`Variable value: ${isMammal} has type: ${typeof isMammal}`);



#### Task 5 💻
Запросить у пользователя имя и возраст и вывести их в консоль.

    console.log(prompt('Enter your name:'));
    console.log(prompt('Enter your age:'));



### ADVANCED level
#### Task 1 👨‍🏫 
Поменяйте значение переменных местами не создавая дополнительной переменной:

    let a = 4;
    let b = 3;

    a -= b;
    b += a;
    a = b - a;

    console.log(a);
    console.log(b);