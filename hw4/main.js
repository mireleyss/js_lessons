// ### NORMAL level
// #### Task 1 💻
// Выполните каждый из следующих пунктов отдельной строкой:
// 1. Создайте пустой объект `profile` (используйте `const`);
// 2. Добавьте в него свойство `name` со значением `John`;
// 3. Добавьте в него свойство `#lib_internal_isAdmin` со значением `true`;
// 4. Измените значение `name` с `John` на `Фёдор`;
// 5. Удалите свойство `#lib_internal_isAdmin`;

const profile = {};
profile.name = 'John';
profile['#lib_internal_isAdmin'] = true;
profile.name = 'Федор';
delete profile['#lib_internal_isAdmin'];



// #### Task 2 🖥
// Дан обьект:
// Вывести в консоль слово красный

const colors = {
    'ru pum pu ru rum': {
        red: 'красный',
        green: 'зеленый',
        blue: 'синий'
    },
};

console.log(colors['ru pum pu ru rum'].red);



// #### Task 3 🖥
// Дан обьект:
// C помощью цикла **for in** вывести в консоль сначала все пары ключ-значение в объекте.
// > name - John age - 19 isHappy - true

const student = {
    name: 'John',
    age: 19,
    isHappy: true
}

for (key in student) {
    console.log(`${key} - ${student[key]}`);
}



// #### Task 4 🖥
// Дан обьект:
// Вычислите среднюю зарплату сотрудников

let salaries = {
    andrey: 500,
    sveta: 413,
    anton: 987,
    alex: 664,
    alexandra: 199
}

let salariesSum = 0;
let counter = 0;

for (salary in salaries) {
    salariesSum += salaries[salary];
    counter++;
}

console.log(salariesSum / counter);



// #### Task 5 🖥
// Напишите функцию `isEmpty()`, которая вернёт `true`, если переданный в неё объект пуст (не содержит свойств).

function isEmpty(object) {
    if (Object.keys(object).length == 0) return true;
    return false;
}

const emptyObj = {};

console.log(isEmpty({ lol: "kek" }));
console.log(isEmpty(emptyObj));



// #### Task 6 🖥
// У нас есть обьект с животными. Наша задача узнать имя птицы и вывести его в консоль. 
// Но произошла ошибка и в этом обьекте нету птицы. Если попробуем узнать имя мы получим **ОШИБКУ**. 
// Задача придумать как обратиться к несуществующему обьекту и не получить ошибку чтобы наша программа работала дальше.

const animals = {
    cat: {
       name: 'Енчик',
       age: 3,
    },
    dog: {
       name: 'Орео',
       age: 2,
    }
 }

 console.log(animals?.bird?.name);



//  #### Task 7 🖥
// Опишите по пунктам, как отработает этот код?
// Важно ответить на вопрос, что будет записано в `this` и почему?

function makeProfile() {
  return {
    name: "John",
    self: this
  };
}

let user = makeProfile();

// alert( user.self.name ); // Каким будет результат?

// в user запишется name и self. В name запишется "John",
// а this будет ссылаться либо на window в браузере, либо на global в node.js
// this будет ссылаться на созданный объект только если оно прописано внутри метода (кроме стрелочных функций, у них нет this)
// в строгом режиме в self запишется undefined, user.self.name выдаст ошибку (у undefined не может быть свойств)



// #### Task 8 🖥
// Создайте объект счётчик `counter`, в котором будет записано значение изначально равное `0`.
// В `counter` должен быть метод `showCurrent()`, который будет выводить в консоль текущее значение.
// Также в объекте должно быть два метода для уменьшения и увеличения значения:


const counter = {
    count: 0,
    showCurrent() {
        console.log(this.count);
    },
    increment() {
        this.count++;
    },
    decrement() {
        this.count--;
    }
}

counter.showCurrent(); // 0 - вывод в консоль
counter.increment();
counter.increment();
counter.showCurrent(); // 2
counter.decrement();
counter.showCurrent(); // 1



// #### Task 9 🖥
// Дополните результат из задания 8 так, чтобы можно было составить цепочку вызовов:

const counterTask9 = {
    count: 0,
    showCurrent() {
        console.log(this.count);
        return this;
    },
    increment() {
        this.count++;
        return this;
    },
    decrement() {
        this.count--;
        return this;
    }
}

counterTask9.showCurrent().increment().increment().showCurrent().decrement().showCurrent();



// #### Task 10 🖥
// Сделайте функцию-конструктор `Counter()`, которая сможет создавать счётчики из заданий 8-9:

function Counter() {
    this.count = 0;

    this.showCurrent = function() {
        console.log(this.count);
        return this;
    }

    this.increment = function() {
        this.count++;
        return this;
    }

    this.decrement = function() {
        this.count--;
        return this;
    }
}

const counter1 = new Counter();
const counter2 = new Counter();

console.log('-------------------');
counter2.increment();
counter2.showCurrent(); // 1
counter1.showCurrent(); // 0
counter1.showCurrent().increment().increment().showCurrent().decrement().showCurrent();